def calculate_tax(dic):
    while True:
        try:
            people = dic.keys()
            for person in people:
                earning = dic[person]
                if earning <= 1000:
                    dic[person] = 0
                elif earning in range(1001, 10001):
                    bracket1 = 0 * 1000
                    bracket2 = 0.1 * (earning - 1000)
                    tax = bracket1 + bracket2
                    dic[person] = tax
                elif earning in range(10001, 20201):
                    bracket1 = 0 * 1000
                    bracket2 = 0.1 * 9000
                    bracket3 = 0.15 * (earning - 10000)
                    tax = bracket1 + bracket2 + bracket3
                    dic[person] = tax
                elif earning in range(20201, 30751):
                    bracket1 = 0 * 1000
                    bracket2 = 0.1 * 9000
                    bracket3 = 0.15 * 10200
                    bracket4 = 0.20 * (earning - 20200)
                    tax = bracket1 + bracket2 + bracket3 + bracket4
                    dic[person] = tax
                elif earning in range(30751, 50001):
                    bracket1 = 0 * 1000
                    bracket2 = 0.1 * 9000
                    bracket3 = 0.15 * 10200
                    bracket4 = 0.20 * 10550
                    bracket5 = 0.25 * (earning - 30750)
                    tax = bracket1 + bracket2 + bracket3 + bracket4 + bracket5
                    dic[person] = tax
                elif earning > 50000:
                    bracket1 = 0 * 1000
                    bracket2 = 0.1 * 9000
                    bracket3 = 0.15 * 10200
                    bracket4 = 0.20 * 10550
                    bracket5 = 0.25 * 19250
                    bracket6 = 0.3 * (earning - 50000)
                    tax = bracket1 + bracket2 + bracket3 + bracket4 + bracket5 + bracket6
                    dic[person] = tax
            return dic
            break
        except (AttributeError, TypeError):
            raise ValueError('The provided input is not a dictionary')

