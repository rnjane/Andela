def binary_converter(num):
    if num == 0:
        return "0"
    elif num > 255:
        return "Invalid input"
    elif num < 0:
        return "Invalid input"
    else:
        binary = ''
        while(num > 0):
            t = num % 2
            binary = str(t) + binary
            num = num / 2
        return binary

print binary_converter(62)

