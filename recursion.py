#iteration
def replicate_iter(times, data):
    if not type(times) == int:
        raise ValueError("Invalid arguments")
    if times == 0 or times < 0:
        return []
    if not (isinstance(data, (int,float,str,complex,long))):
        raise ValueError("Invalid arguments")
    else:
        array = []
        for i in range(0, times):
            array.append(data)
        return array

#recursion
def replicate_recur(times,data):
    if not type(times) == int:
        raise ValueError("Invalid arguments")
    if not (isinstance(data, (int,float,str,complex,long))):
        raise ValueError("Invalid arguments")
    if times == 0 or times < 0:
        return []
    else:
        return ([data]+replicate_iter((times - 1),data)) 