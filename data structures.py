def manipulate_data(l):
    pnum = 0
    nnum = 0

    if isinstance(l,list):
        for i in l:
            if i >= 0:
                pnum += 1
            elif i < 0:
                nnum = nnum + i

        return [pnum, nnum]
    else:
        return "Only lists allowed"

y = [1, 2, 3, 4]

print manipulate_data(y)
